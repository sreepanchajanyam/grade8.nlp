# README #

1. Running Chunking Logic on training_set.tsv
2. Extracting content  from DBpedia for the above chunks
3. Running chunking on DBPedia content to come with tags
4. Ingesting Data in the following format 
{ tags: {},
   content:{}
} 
5. Merging Chunks  - Discuss
6. Relation between data - Discuss
7. Encoding data into numeric format- Discuss
8. Coming up with format for http://deeplearning4j.org/word2vec.html
9. Percy liang's https://github.com/percyliang/sempre
10. Information extraction vs Semantic parsing: http://allenai.org/content/publications/ie-vs-sp.pdf
11. Answer sentence selection with Deep Learning: http://arxiv.org/pdf/1412.1632v1.pdf
12. Knowledge networks: http://arxiv.org/pdf/1501.03471v1.pdf
13. http://arxiv.org/pdf/1506.01094.pdf Composition for cascaded inference over knowledge graphs.

## Steps in processing
The following are the two steps in understanding the question and checking for suitabile answers.

### Question understanding

The various questions in the training set can be categorized into the following ways:

+ Questions where each choice is a complete sentence that can be evaluated to fetch the answer.
    - Consider this question 
        - *100686	Which of the following statements best describes visible light?	C	
        - A. Light is an electromagnetic wave of a single frequency that makes up a small part of the electromagnetic spectrum.	
        - B. Light is an electromagnetic wave of a single frequency that makes up most of the electromagnetic spectrum.	
        - C. Light includes a range of electromagnetic waves that make up a small part of the electromagnetic spectrum.	
        - D. Light includes a range of electromagnetic waves that make up most of the electromagnetic spectrum.*
    An assertion of the truth values of the above statement will itself contain the right answer.
+ Questions where the choices are to be completed to full sentences before any assertions of truth can be made.
    - Consider this question
        - *100664	Which information is used in a station model?	D	
        - A. yearly average precipitation	
        - B. daily average barometric pressure	
        - C. number of lightning strikes	
        - D. wind direction in a particular point and time*
    Choices must be completed to sentences as in "Yearly average precipitation is used in a station model." before an assertion of truth value can be made.
    - Questions with blanks need to completed similarly.
        - *100683	When calculating distance in space, planets appear to move __________ stars.	B	
        - A. at the same speed as	
        - B. faster than	
        - C. in different directions than	
        - D. slower than*
        We must make assertion an on "When calculating distance in space, planets appear to move slower than stars".
+ There are other questions like this one
    - *100666	Which sequence lists the levels of organization in the human body from simplest to most complex?	D	
    - A. organ system -> tissue -> cell -> organ	
    - B. tissue -> cell -> organ -> organ system	
    - C. organ -> organ system -> tissue -> cell	
    - D. cell -> tissue -> organ -> organ system*

Interpreting and understanding the question is in itself quite a task.
The simplest is to rewrite the question along with each of the choices to make a valid sentence that can be asserted for truth value.
However, the resulting intermediate format should contain *exactly the same information* as in the question and the choice.

### Choice evaluation

+ Labeled data is limited and is unlikely to be sufficient to train a classifier, 
however, the simplest to try will be a binary classifier to evaluate correctness.
This might include multiple feature extraction and preprocessing methods like 
word2vec, cca, etc.
Also there is no easy way to integrate openly available data into this method.
+ An alternative is to use the wealth of data available from the web and mine for
various facts and put them in a Markov Logic Network or Sum Product Network and 
leverage it to draw validity of inferences.

Finally we might need a combination of the above methods.