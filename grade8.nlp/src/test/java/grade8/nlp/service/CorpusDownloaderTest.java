package grade8.nlp.service;

import grade8.nlp.download.DBPediaAbstractDownloader;
import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

public class CorpusDownloaderTest {
	
	//@Test
	public void downloadCorpusForTokens() throws FileNotFoundException, IOException, InterruptedException, ExecutionException{

		List<String> filterTagsList = new ArrayList<>();
		filterTagsList.add("NN");
		filterTagsList.add("NNS");
		filterTagsList.add("NNP");
		filterTagsList.add("NNPS");
		TokenFilter tokenFilter = new TagFilterForTokens(filterTagsList);
		CorpusDownloader corpusDownloader = new CorpusDownloader(new DBPediaAbstractDownloader());
		corpusDownloader.downloadCorpusForTokens(new FileInputStream("data/some_sentences"), new FileOutputStream("data/some_sentences_corpus"), tokenFilter);

	}
}
