package grade8.nlp.service;

import grade8.nlp.Util;
import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.util.FileUtil;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.junit.Test;

public class CorpusToBitSetMapperTest {
	private static Logger logger = Logger.getLogger(CorpusToBitSetMapperTest.class);
	@Test
	public void testGetBitSetArray() throws ClassNotFoundException, IOException {
		InputStream sentenceCorpusInputStream = Util.getResourceAsStream("data/test_dbpedia");
		InputStream wordToClusterInputStream = Util.getResourceAsStream("data/test_set.out");
		
		
		SentenceToBitSetMap[] sentenceToBitSetMapArr = CorpusToBitSetMapper.getBitSetArray(sentenceCorpusInputStream, wordToClusterInputStream);
		for(SentenceToBitSetMap sentencetoBitSet :  sentenceToBitSetMapArr){
			if(logger.isTraceEnabled()) logger.trace(sentencetoBitSet);
		}
		try(OutputStream fous = new FileOutputStream("/home/centos/bitset")){
			FileUtil.writeAsString(sentenceToBitSetMapArr, fous);
		}
	}
}
