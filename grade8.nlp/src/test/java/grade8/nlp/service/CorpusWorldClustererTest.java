package grade8.nlp.service;

import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.util.FileUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Runs only on Ubuntu 14.0.4 due to JBlas dependency on native libraries.
 * @author Sree Panchajanyam D
 *
 */
public class CorpusWorldClustererTest {
	
	//@Test
	public void generateWordClusterMapFromCorpus() throws FileNotFoundException, IOException, InterruptedException, ExecutionException{
		List<String> filterTagsList = new ArrayList<>();
		filterTagsList.add("NN");
		filterTagsList.add("NNS");
		filterTagsList.add("NNP");
		filterTagsList.add("NNPS");

		filterTagsList.add("VB");
		filterTagsList.add("VBD");
		filterTagsList.add("VBN");
		filterTagsList.add("VBP");
		filterTagsList.add("VBZ");

		filterTagsList.add("JJ");
		filterTagsList.add("JJR");
		filterTagsList.add("JJS");

		filterTagsList.add("WDT");
		filterTagsList.add("WP");
		filterTagsList.add("WP$");
		filterTagsList.add("RBR");
		filterTagsList.add("RBS");
		
		TokenFilter tokenFilter = new TagFilterForTokens(filterTagsList);
		Map<String, Integer> word2ClusterMap= CorpusWordClusterer.getWordClusterMapFromCorpus(new FileInputStream("data/test_set"),tokenFilter);
		FileUtil.writeAsObject((Serializable) word2ClusterMap,  new FileOutputStream("data/test_set_clustermap"));

	}

}
