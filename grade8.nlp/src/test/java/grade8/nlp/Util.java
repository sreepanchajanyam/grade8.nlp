package grade8.nlp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Util {

	
	public static InputStream getResourceAsStream(String fileName) {
		return Util.class.getClassLoader().getResourceAsStream(fileName);
		
	}
	
	public static InputStream getFileAsStream(File file) throws FileNotFoundException {
		return new FileInputStream(file);
		
	}
	
}
