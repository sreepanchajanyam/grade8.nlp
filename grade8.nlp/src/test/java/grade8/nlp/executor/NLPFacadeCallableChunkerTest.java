package grade8.nlp.executor;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.executor.ChunkerExecutor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.junit.Test;



public class NLPFacadeCallableChunkerTest {
	
	private final static Logger logger = Logger.getLogger(NLPFacadeCallableChunkerTest.class);
	
	
	@Test
	public void testGetChunksThreaded() throws IOException, InterruptedException, ExecutionException {
		String[] sentences = SentenceDetector.getSentences(getResourceAsStream("data/test_set"));
		List<String> chunkTypes = null;
		List<String> allChunks = new ArrayList<>();
		ExecutorService executor = Executors.newCachedThreadPool();
		List<Future<String[]>> futureList = new ArrayList<Future<String[]>>();
		for (String sentence : sentences) {
			Future<String[]> future = executor.submit(new ChunkerExecutor(sentence, chunkTypes));
			futureList.add(future);
			
		} 
		
		for(Future<String[]> future : futureList ){
			allChunks.addAll(Arrays.asList(future.get()));
		}
		logger.info("All Chunks:" + allChunks);
		executor.shutdown();
	} 
	
	
	public static InputStream getResourceAsStream(String fileName) {
		return NLPFacadeCallableChunkerTest.class.getClassLoader().getResourceAsStream(fileName);

	}
}
