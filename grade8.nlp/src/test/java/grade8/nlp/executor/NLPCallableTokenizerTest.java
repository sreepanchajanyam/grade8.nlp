package grade8.nlp.executor;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.executor.TokenizerExecutor;
import grade8.nlp.filter.TagFilterForTokens;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.junit.Test;

public class NLPCallableTokenizerTest {
	
	
	private final static Logger logger = Logger.getLogger(NLPCallableTokenizerTest.class);
	
	@Test
	public void testExecute() throws IOException, InterruptedException, ExecutionException{
		
		List<String> tagList = new ArrayList<>();
		tagList.add("NN");
		tagList.add("NNS");
		tagList.add("NNP");
		tagList.add("NNPS");
		
		tagList.add("VB");
		tagList.add("VBD");
		tagList.add("VBN");
		tagList.add("VBP");
		tagList.add("VBZ");
		
		tagList.add("JJ");
		tagList.add("JJR");
		tagList.add("JJS");
		
		tagList.add("WDT");
		tagList.add("WP");
		tagList.add("WP$");	
		tagList.add("RBR");
		tagList.add("RBS");
		
		
		String[] sentences = SentenceDetector.getSentences(NLPFacadeCallableChunkerTest.getResourceAsStream("data/test_set"));
		List<String> allTokens = new ArrayList<>();
		
		ExecutorService executor = Executors.newFixedThreadPool(20);
		Future<String[]>[] futureList = new Future[sentences.length];
		for (int i=0; i< sentences.length; i++) {
			TagFilterForTokens tagFilterForTokens = new TagFilterForTokens(tagList);
			Future<String[]> future = executor.submit(new TokenizerExecutor(sentences[i], tagFilterForTokens));
			futureList[i] = future;
			
		} 
		
		for(Future<String[]> future : futureList ){
			allTokens.addAll(Arrays.asList(future.get()));
		}
		logger.info("All Chunks:" + allTokens);
		executor.shutdown();
	}

}
