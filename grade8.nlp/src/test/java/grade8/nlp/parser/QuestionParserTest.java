package grade8.nlp.parser;

import org.junit.Test;

public class QuestionParserTest {
	
	
	String str = "100003	When two nuclei are combined into one nucleus, there is a slight change in mass and the release of a large amount of energy. What is this process called?	D	conversion	reaction	fission	fusion";
	String str1 = "100023	Which behavior can help animals establish a territory?	C	A caribou herd migrates seasonally.	A scent trail leads ants to a food source.	A wolf pack howls to warn other wolves to stay away.	A honeybee society divides the work between its members.";
	@Test
	public void testParse(){
		String[] tokens = QuestionParser.parse(str1);
		for(String token : tokens){
			System.out.println(token);
		}
	}
}
