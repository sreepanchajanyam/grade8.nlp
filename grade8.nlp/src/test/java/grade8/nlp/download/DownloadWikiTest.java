package grade8.nlp.download;

import grade8.nlp.download.DBPediaAbstractDownloader;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.Test;

public class DownloadWikiTest {

	private Logger logger = Logger.getLogger(DownloadWikiTest.class);
	@Test
	public void testDownloadDBPediaAbstract() throws IOException{ 
		DBPediaAbstractDownloader dbPediaExtractor = new DBPediaAbstractDownloader();
		logger.info(dbPediaExtractor.download("Bacteria"));
	}
}
