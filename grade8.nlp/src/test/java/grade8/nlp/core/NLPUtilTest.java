package grade8.nlp.core;

import grade8.nlp.Util;
import grade8.nlp.consts.ChunkTypes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

public class NLPUtilTest {

	private Logger logger = Logger.getLogger(grade8.nlp.core.NLPUtilTest.class);

	/**
	 * Heavy processing file don't use this for tests
	 * 
	 * @throws IOException
	 */

	@Test
	public void testGetSentencesWithTestData() throws IOException {
		String[] sentences = SentenceDetector.getSentences(Util.getResourceAsStream("data/test_set"));
		logger.info(" Sentences: " + Arrays.toString(sentences));
	}

	@Test
	public void testGetTokensWithTestData() throws Exception {

		String[] sentences = SentenceDetector.getSentences(Util.getResourceAsStream("data/test_set"));
		List<String> tokens = new ArrayList<>();
		for (String sentence : sentences) {
			Tokenizer tokenizer = new Tokenizer(sentence);
			tokens.addAll(Arrays.asList(tokenizer.getTokens()));
		}
		logger.info("All tokens : " + tokens);
	}

	@Test
	public void testGetPOSWithTestData() throws Exception {

		String[] sentences = SentenceDetector.getSentences(Util.getResourceAsStream("data/test_set"));
		List<String> posTags = new ArrayList<>();
		for (String sentence : sentences) {
			Tokenizer tokenizer = new Tokenizer(sentence);
			Tagger tagger = new Tagger(tokenizer.getTokens());

			posTags.addAll(Arrays.asList(tagger.getPOSTags()));
		}

		logger.info("All POS Tags : " + posTags);
	}

	@Test
	public void testGetChunksWithTestData() throws Exception {
		String[] sentences = SentenceDetector.getSentences(Util.getResourceAsStream("data/test_set"));
		List<String> chunkList = new ArrayList<>();
		List<String> chunkTypes = null;
		for (String sentence : sentences) {
			Tokenizer tokenizer = new Tokenizer(sentence);
			String[] tokens = tokenizer.getTokens();
			Tagger tagger = new Tagger(tokens);
			String[] posTags = tagger.getPOSTags();
			Chunker chunker = new Chunker(tokens, posTags, chunkTypes);
			chunkList.addAll(Arrays.asList(chunker.getChunks()));
		}

		logger.info("All Chunks  : " + chunkList);
	}
	
	@Test
	public void testGetSpecificChunksWithTestData() throws Exception {
		String[] sentences = SentenceDetector.getSentences(Util.getResourceAsStream("data/test_set"));
		List<String> chunkList = new ArrayList<>();
		List<String> chunkTypes = new ArrayList<>();
		chunkTypes.add(ChunkTypes.NOUN_PHRASE.getValue());
		for (String sentence : sentences) {
			Tokenizer tokenizer = new Tokenizer(sentence);
			String[] tokens = tokenizer.getTokens();
			Tagger tagger = new Tagger(tokens);
			String[] posTags = tagger.getPOSTags();
			Chunker chunker = new Chunker(tokens, posTags, chunkTypes);
			chunkList.addAll(Arrays.asList(chunker.getChunks()));
		}

		logger.info("All Chunks  : " + chunkList);
	}

	
	private void printTable(String[] tokens, String[] pos, String[] chunks) {
		System.out.println(("tokens\t \t \t \t \t" + "pos\t \t \t \t \t" + "chunks"));
		for (int i = 0; i < tokens.length; i++) {
			System.out.println((tokens[i] + "\t \t \t \t \t" + pos[i] + "\t \t \t \t \t" + chunks[i]));

		}
	}
}
