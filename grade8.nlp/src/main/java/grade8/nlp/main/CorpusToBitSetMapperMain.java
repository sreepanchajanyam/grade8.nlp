package grade8.nlp.main;

import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.service.CorpusToBitSetMapper;

import java.io.FileInputStream;
import java.util.BitSet;

import org.apache.log4j.Logger;

public class CorpusToBitSetMapperMain {
	
	private static final Logger logger = Logger.getLogger(CorpusToBitSetMapperMain.class);
	
	public static void main(String[] args) throws Exception {
		
		FileInputStream sentenceCorpusInputStream = null;
		FileInputStream wordToClusterInputStream = null;
		int dimension = 500;
		if (args[0] != null && !"".equals(args[0].trim())) {
			sentenceCorpusInputStream = new FileInputStream(args[0]);
		} else {
			logger.info("usage : java -jar <jar_name> corpus_filename word2clustermap_filename [CLUSTER_DIMENSION_INT_VALUE]");
			throw new Exception("Corpus File Name");
		}

		if (args[1] != null && !"".equals(args[1].trim())) {
			wordToClusterInputStream = new FileInputStream(args[0]);
		} else {
			logger.info("usage : java -jar <jar_name> corpus_filename word2clustermap_filename [CLUSTER_DIMENSION_INT_VALUE]");
			sentenceCorpusInputStream.close();
			throw new Exception("word to cluster map File Name");
		}
		
		try {
			if(args[2] != null && !"".equals(args[1].trim())) {
				dimension = Integer.parseInt(args[2]);
			}
		} catch(NumberFormatException nfe){
			logger.error("Invalid number in cluster dimension, args[2] : " + args[2] + " Defaulting cluster dimension to 500" );
		}
		

		for (SentenceToBitSetMap sentenceToBitSetMap : CorpusToBitSetMapper.getBitSetArray(sentenceCorpusInputStream, wordToClusterInputStream)) {
			logger.info(sentenceToBitSetMap+ "\n");
		}
		sentenceCorpusInputStream.close();
		wordToClusterInputStream.close();
	}

}
