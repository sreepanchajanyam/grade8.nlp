package grade8.nlp.main;

import grade8.nlp.filter.StopWordFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.filter.WordCleanserForTokens;
import grade8.nlp.util.FileUtil;

import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * Tokenizes an input file that keywords \t content
 * 
 * @author Sree Panchajanyam D
 * 
 */

public class TokenFilterMain {

	private static Integer MIN_FREQUENCY = 5;
	private static Logger logger = Logger.getLogger(TokenFilterMain.class);

	public static void main(String[] args) throws Exception {

		String inputfile = args[0];
		String outputfile = args[1];


		TokenFilter duplicateWordFilter = new WordCleanserForTokens();
		TokenFilter stopwordsFilter = new StopWordFilterForTokens(Arrays.asList(FileUtil.getString(FileUtil.getInputStream("models/en-stopwords")).split(" ")));

		long startTime = System.nanoTime();
		System.out.println("Started tokenizing...");
		List<String> tokenList = Files.readAllLines(Paths.get(inputfile), Charset.defaultCharset());
		String[] tokens = duplicateWordFilter.filter(tokenList.toArray(new String[tokenList.size()]));
		tokens = stopwordsFilter.filter(tokens);
		StringBuilder sb = new StringBuilder();
		for(String token : tokens) {
			sb.append(token).append("\n");
		}
		
		logger.info("Tokenizing Done, time taken complete executor = " + (System.nanoTime() - startTime) / 1000000000L);
		startTime = System.nanoTime();
		logger.info(outputfile);
		FileUtil.writeAsString(sb, new FileOutputStream(outputfile));
		logger.info("Tokenizing Done, time taken to write to file= " + (System.nanoTime() - startTime) / 1000000000L);
	}

}
