package grade8.nlp.main;

import grade8.nlp.core.AnswerOptionsDetector;
import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.download.CorpusDoc;
import grade8.nlp.download.WikiESContentDownloader;
import grade8.nlp.score.strategy.ESnBitSetScoringStrategy;
import grade8.nlp.score.strategy.ScoringStrategy;
import grade8.nlp.service.CorpusToBitSetMapper;
import grade8.nlp.util.FileUtil;
import grade8.nlp.util.PropertyUtil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.BitSet;
import java.util.Map;

import org.apache.log4j.Logger;

public class SubmissionMainW2VecES {
	private final static String QUESTION_OPTION_TEXT = "questions";
	private final static String ANSWER_FILE = "answerfile.out";
	private final static String WORD_CLUSTER_MAP_FILE = "wordclustermapfile.out";
	private final static String ES_INDICES = "indices";
	private final static String ES_TYPE = "type";
	private final static String ES_LIMIT = "limit";
	private final static String STRATEGY = "strategy";

	private static String questionOptionFile;
	private static String wordClusterMapFileName;
	private static String answerFile;

	private static final Logger logger = Logger.getLogger(SubmissionMainW2VecES.class);

	public static void main(String[] args) throws Exception {

		questionOptionFile = PropertyUtil.getStringProperty(QUESTION_OPTION_TEXT,"/home/centos/training_fulltext_token_nn_vb_jj_rb_no_stopwords.txt");
		answerFile = PropertyUtil.getStringProperty(ANSWER_FILE,"/home/centos/w2v_es_training_answerfile");
		wordClusterMapFileName = PropertyUtil.getStringProperty(WORD_CLUSTER_MAP_FILE,"/home/centos/git/grade8.nlp/grade8.nlp/src/main/resources/models/clean_words_clustermap_1200.bin");
		String[] indices = PropertyUtil.getStringProperty(ES_INDICES,"wiki,ck12").split(",");
		String type = PropertyUtil.getStringProperty(ES_TYPE,"fact_pill");
		int limit = PropertyUtil.getIntProperty(ES_LIMIT, 10);
		String strategy = PropertyUtil.getStringProperty(STRATEGY, "AVG");
		
		Map<String, Integer> wordToClusterMap = (Map<String, Integer>) FileUtil.readObject(new FileInputStream(wordClusterMapFileName));
		WikiESContentDownloader contentDownloader = new WikiESContentDownloader(indices, type, limit);
		StringBuilder sb = new StringBuilder();
		sb.append("id").append(",correctAnswer\n");
		try (BufferedReader tcReader = new BufferedReader(new FileReader(questionOptionFile))) {
			String sentence = null;
			while ((sentence = tcReader.readLine()) != null) {
				String[] options = AnswerOptionsDetector.getOptions(sentence);
				String questionID = options[0];
				float maxValue = -1.0f;
				int bestfitOption = -1;
				for (int i = 1; i < 5; i++) {
					CorpusDoc doc = contentDownloader.getDocuments(options[i]);
					BitSet bitSet = CorpusToBitSetMapper.getBitSetFromSentence(options[i], wordToClusterMap);
					ScoringStrategy<BitSet> scoringStrategy = new ESnBitSetScoringStrategy(doc.getCorpus(),doc.getScore(),ESnBitSetScoringStrategy.AVG_STRATEGY);
				    scoringStrategy.setQuestionID(questionID);
				    scoringStrategy.setOptionID(getOption(i));
					float value = scoringStrategy.calculateScore(bitSet);
					if (value > maxValue) {
						bestfitOption = i;
						maxValue = value;
					} else if (value == maxValue && bestfitOption != -1) {
						logger.info("option" + i + " = " + " option" + bestfitOption);
					}
				}
				sb.append(questionID).append(",").append(getOption(bestfitOption)).append("\n");
			}
		}
		FileUtil.writeAsString(sb, new FileOutputStream(answerFile));
		
	}
	private static String getOption(int i){
		char option = (char) ('A' + (i-1));
		return String.valueOf(option);
	}

}
