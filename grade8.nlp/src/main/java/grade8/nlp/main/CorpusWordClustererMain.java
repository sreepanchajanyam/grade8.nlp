package grade8.nlp.main;

import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.service.CorpusWordClusterer;
import grade8.nlp.util.FileUtil;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class CorpusWordClustererMain {

	private final static Logger logger = Logger.getLogger(CorpusWordClustererMain.class);
	private final static String DEFAULT_OUTPUT_FILE = "word2vec.out";

	public static void main(String[] args) throws Exception {
		String outputFile = DEFAULT_OUTPUT_FILE;
		String inputFile = null;
		if (args == null || args.length < 2) {
			logger.info("usage : java -jar <jar_name> input_filename output_filename -Dclustermode=<path_to_deps.words> [-Dnumclusters=int] [-Dnumtrials=int] [-Dnumiters=int]");
			throw new Exception("Input File Name and Output File Name are expected ");
		}
		
		if (args[0] != null && !"".equals(args[0].trim())) {
			inputFile = args[0];
		} else {
			logger.info("usage : java -jar <jar_name> input_filename output_filename -Dclustermode=<path_to_deps.words> [-Dnumclusters=int] [-Dnumtrials=int] [-Dnumiters=int]");
			throw new Exception("Corpus File Name");
		}
		if (args[1] != null && !"".equals(args[1].trim())) {
			outputFile = args[1];
		}
		
		List<String> filterTagsList = new ArrayList<>();
		filterTagsList.add("NN");
		filterTagsList.add("NNS");
		filterTagsList.add("NNP");
		filterTagsList.add("NNPS");

		filterTagsList.add("VB");
		filterTagsList.add("VBD");
		filterTagsList.add("VBN");
		filterTagsList.add("VBP");
		filterTagsList.add("VBZ");

		filterTagsList.add("JJ");
		filterTagsList.add("JJR");
		filterTagsList.add("JJS");

		filterTagsList.add("WDT");
		filterTagsList.add("WP");
		filterTagsList.add("WP$");
		filterTagsList.add("RBR");
		filterTagsList.add("RBS");
		
		TokenFilter tokenFilter = new TagFilterForTokens(filterTagsList);
		Map<String, Integer> word2ClusterMap= CorpusWordClusterer.getWordClusterMapFromCorpus(new FileInputStream(inputFile),tokenFilter);
		FileUtil.writeAsObject((Serializable) word2ClusterMap,  new FileOutputStream(outputFile));
	}
	

}
