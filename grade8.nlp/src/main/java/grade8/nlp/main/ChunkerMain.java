package grade8.nlp.main;

import grade8.nlp.executor.ChunkerExecutor;
import grade8.nlp.util.FileUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ChunkerMain {
	
	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException, ExecutionException {
		//String[] sentences = SentenceDetector.getSentences(new FileInputStream(new File("/home/centos/Desktop/ck12.txt")));
		BufferedReader reader = new BufferedReader(new FileReader(new File("/home/centos/Desktop/ck12.txt")));
		String line = null;
		List<String> list = new ArrayList<>();
		while ((line = reader.readLine()) != null)
			list.add(line);
		String[] sentences = list.toArray(new String[list.size()]);
		StringBuilder sb = new StringBuilder();
		ExecutorService executorService = Executors.newFixedThreadPool(20);
		Future<String[]>[] futureArr = new Future[sentences.length];
		List<String> chunkList = new ArrayList<>();
		chunkList.add("NP");
		//chunkList.add("VP");
		for(int i=0; i < sentences.length; i++) {
			futureArr[i] = executorService.submit(new ChunkerExecutor(sentences[i],chunkList));
		}
		for(Future<String[]> future : futureArr) {
			String[] chunks = future.get();
			for(String chunk : chunks) {
				sb.append(chunk).append("\n");
			}
		}
		
		FileUtil.writeAsString(sb, new FileOutputStream(new File("/home/centos/Desktop/ck12_nn_chunks_on_sentence.txt")));
		executorService.shutdown();
	}

}
