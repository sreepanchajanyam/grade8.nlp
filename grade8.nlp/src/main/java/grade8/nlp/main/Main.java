package grade8.nlp.main;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.download.DBPediaAbstractDownloader;
import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.service.CorpusDownloader;
import grade8.nlp.service.CorpusToBitSetMapper;
import grade8.nlp.service.CorpusWordClusterer;
import grade8.nlp.util.FileUtil;
import grade8.nlp.util.PropertyUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Main {

	private final static String TRAINING_CORPUS_INPUT_FILE = "tcfile.in";
	private final static String KNOWLEDGE_CORPUS_OUPUT_FILE = "kcfile.out";
	private final static String TRAINING_CORPUS_BIT_SET_FILE = "tcbitsetfile.out";
	private final static String KNOWLEDGE_CORPUS_BIT_SET_FILE = "kcbitsetfile.out";
	private final static String WORD_CLUSTER_MAP_FILE = "wordclustermapfile.out";

	private static String tcFileName;
	private static String kcFileName;
	private static String tcBitSetFileName;
	private static String kcBitSetFileName;
	private static String wordClusterMapFileName;

	public static void main(String[] args) throws Exception {

		tcFileName = PropertyUtil.getStringProperty(TRAINING_CORPUS_INPUT_FILE);
		kcFileName = PropertyUtil.getStringProperty(KNOWLEDGE_CORPUS_OUPUT_FILE);
		tcBitSetFileName = PropertyUtil.getStringProperty(TRAINING_CORPUS_BIT_SET_FILE);
		kcBitSetFileName = PropertyUtil.getStringProperty(KNOWLEDGE_CORPUS_BIT_SET_FILE);
		wordClusterMapFileName = PropertyUtil.getStringProperty(WORD_CLUSTER_MAP_FILE);

		List<String> filterTagsList = new ArrayList<>();
		filterTagsList.add("NN");
		filterTagsList.add("NNS");
		filterTagsList.add("NNP");
		filterTagsList.add("NNPS");
		TokenFilter tokenFilter = new TagFilterForTokens(filterTagsList);


		String[] trainingCorpus = null;
		String[] knowledgeCorpus = null;
		FileInputStream training_corpus_is = null;
		FileInputStream knowledge_corpus_is = null;

		try {
			training_corpus_is = new FileInputStream(tcFileName);
			trainingCorpus = SentenceDetector.getSentences(training_corpus_is);
			saveKnowledgeCorpus(trainingCorpus, kcFileName,tokenFilter); // Download and save knowledge corpus
			knowledge_corpus_is = new FileInputStream(kcFileName);
			knowledgeCorpus = SentenceDetector.getSentences(knowledge_corpus_is);
		} finally {
			if (training_corpus_is != null) training_corpus_is.close();
			if (knowledge_corpus_is != null) knowledge_corpus_is.close();
		}

		
		Map<String, Integer> wordClusterMap = getWordClusterMap(knowledgeCorpus, tokenFilter); // cluster words in knowledge corpus
		generateNSaveBitSet(trainingCorpus, wordClusterMap, tcBitSetFileName); // Generate BitSet on trainingCorpus and save to a file
		generateNSaveBitSet(knowledgeCorpus, wordClusterMap, kcBitSetFileName); // Generate BitSet on knowledgeCorpus and save to a file
	}
	
	/**
	 * 1. Chunk the file tcFileName into word tokens. 2. Download the
	 * knowledge_corpus for each token 3. Save the knowledge_corpus to the file
	 * kcFileName
	 */

	private static void saveKnowledgeCorpus(String[] trainingCorpus, String kcFileName, TokenFilter tokenFilter) throws FileNotFoundException, IOException, InterruptedException, ExecutionException {
		if (!FileUtil.doesFileExist(kcFileName)) {
			CorpusDownloader corpusDownloader = new CorpusDownloader(new DBPediaAbstractDownloader());
			try (FileOutputStream knowledge_corpus_os = new FileOutputStream(kcFileName)) {
				corpusDownloader.downloadCorpusForTokens(trainingCorpus, knowledge_corpus_os, tokenFilter);
			}
		}

	}

	/**
	 * 1. Cluster words in the file kcFileName
	 * 
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */

	private static Map<String, Integer> getWordClusterMap(String[] knowledgeCorpus, TokenFilter tokenFilter) throws IOException, InterruptedException, ExecutionException, ClassNotFoundException {
		Map<String, Integer> wordClusterMap = null;
		if (!FileUtil.doesFileExist(wordClusterMapFileName)) {
			wordClusterMap = CorpusWordClusterer.getWordClusterMapFromCorpus(knowledgeCorpus, tokenFilter);
		} else {
			wordClusterMap = (Map<String, Integer>) FileUtil.readObject(new FileInputStream(wordClusterMapFileName));
		}
		try(FileOutputStream fout = new FileOutputStream(wordClusterMapFileName)){
			FileUtil.writeAsObject((Serializable) wordClusterMap, fout);
		}
		return wordClusterMap;
	}

	private static void generateNSaveBitSet(String[] corpus, Map<String, Integer> wordClusterMap, String outputFile) throws ClassNotFoundException, IOException {

		SentenceToBitSetMap[] bitSetMapArr = CorpusToBitSetMapper.getBitSetArray(corpus, wordClusterMap);
		try (FileOutputStream bitSet_os = new FileOutputStream(outputFile)) {
			FileUtil.writeAsString(bitSetMapArr, bitSet_os);
		}
		try (FileOutputStream bitSet_bin_os = new FileOutputStream(outputFile+".bin")) {
			FileUtil.writeAsObject(bitSetMapArr, bitSet_bin_os);
		}

	}
	
	
}
