package grade8.nlp.main;

import grade8.nlp.executor.TokenizerExecutor;
import grade8.nlp.filter.StopWordFilterForTokens;
import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.util.FileUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Tokenizer for a file with the following format. Delimited by TAB
 * QuestionID	 OptionA		OptionB		OptionC		OptionD
 *  100001	at the tissue level athletes begin to exercise, their heart rates and respiration rates increase. At at the tissue level level of organization does the human body coordinate these functions?	at the organ level athletes begin to exercise, their heart rates and respiration rates increase. At at the organ level level of organization does the human body coordinate these functions?	at the system level athletes begin to exercise, their heart rates and respiration rates increase. At at the system level level of organization does the human body coordinate these functions?	at the cellular level athletes begin to exercise, their heart rates and respiration rates increase. At at the cellular level level of organization does the human body coordinate these functions?
 * 
 * @author centos
 *
 */

public class TokenizeAnswersMain {

	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {

		List<String> filterTagsList = new ArrayList<>();
		filterTagsList.add("NN");
		filterTagsList.add("NNS");
		filterTagsList.add("NNP");
		filterTagsList.add("NNPS");
		filterTagsList.add("VB");
		filterTagsList.add("VBD");
		filterTagsList.add("VBG");
		filterTagsList.add("VBN");
		filterTagsList.add("VBP");
		filterTagsList.add("VBZ");
		filterTagsList.add("RB");
		filterTagsList.add("RBR");
		filterTagsList.add("RBS");
		filterTagsList.add("JJ");
		filterTagsList.add("JJR");
		filterTagsList.add("JJS");
		TokenFilter tagFilter = new TagFilterForTokens(filterTagsList);
		TokenFilter stopwordsFilter = new StopWordFilterForTokens(Arrays.asList(FileUtil.getString(FileUtil.getInputStream("models/en-stopwords")).split(" ")));
		Scanner scanner = new Scanner(FileUtil.getInputStream("data/processed_validation_set_fulltext.tsv"));
		String line = null;
		String questionDetail[] = null;
		ExecutorService executorService = Executors.newFixedThreadPool(4);
		StringBuilder sb = new StringBuilder();
		while (scanner.hasNextLine()) {
			line = scanner.nextLine().replace("\"", "");
			questionDetail = line.split("\t");
			sb.append(questionDetail[0]);
			Future<String[]>[] futArr = new Future[questionDetail.length - 1];
			for (int i = 1; i < questionDetail.length; i++) {
				futArr[i - 1] = executorService.submit(new TokenizerExecutor(questionDetail[i], stopwordsFilter, tagFilter));
			}
			for (Future<String[]> future : futArr) {
				sb.append("\t");
				Set<String> set = new HashSet<String>();
				for (String str : future.get()) {
					if (!set.contains(str)) {
						set.add(str);
						sb.append(str).append(" ");
					}
				}
			}
			sb.append("\n");
		}

		FileUtil.writeAsString(sb, new FileOutputStream(new File("/home/centos/validation_fulltext_token_nn_vb_jj_rb_no_stopwords.txt")));
		System.out.println(sb);
		executorService.shutdown();
	}

}
