package grade8.nlp.main;

import grade8.nlp.core.AnswerOptionsDetector;
import grade8.nlp.core.SentenceDetector;
import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.score.strategy.BitSetMaxDotProduct;
import grade8.nlp.service.CorpusToBitSetMapper;
import grade8.nlp.util.FileUtil;
import grade8.nlp.util.PropertyUtil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.BitSet;
import java.util.Map;

import org.apache.log4j.Logger;

public class SubmissionMain {
	private final static String TRAINING_CORPUS_INPUT_FILE = "tcfile.in";
	private final static String ANSWER_FILE = "answerfile.out";
	private final static String KNOWLEDGE_CORPUS_BIT_SET_FILE = "kcbitsetfile.out";
	private final static String WORD_CLUSTER_MAP_FILE = "wordclustermapfile.out";
	private final static String CLUSTER_DIMENSION = "numclusters.int";

	private static String tcFileName;
	private static String kcBitSetFileName;
	private static String wordClusterMapFileName;
	private static String answerFile;
	private static int numclusters;

	private static final Logger logger = Logger.getLogger(SubmissionMain.class);

	public static void main(String[] args) throws Exception {

		tcFileName = PropertyUtil.getStringProperty(TRAINING_CORPUS_INPUT_FILE);
		kcBitSetFileName = PropertyUtil.getStringProperty(KNOWLEDGE_CORPUS_BIT_SET_FILE);
		answerFile = PropertyUtil.getStringProperty(ANSWER_FILE);
		wordClusterMapFileName = PropertyUtil.getStringProperty(WORD_CLUSTER_MAP_FILE);
		numclusters = PropertyUtil.getIntProperty(CLUSTER_DIMENSION, 500);

		SentenceToBitSetMap[] kcBitSet = (SentenceToBitSetMap[]) FileUtil.readObject(new FileInputStream(kcBitSetFileName));

		BitSetMaxDotProduct corpusDotProdCalc = new BitSetMaxDotProduct(kcBitSet);
		Map<String, Integer> wordToClusterMap = (Map<String, Integer>) FileUtil.readObject(new FileInputStream(wordClusterMapFileName));
		StringBuilder sb = new StringBuilder();
		sb.append("id").append("correctAnswer");
		try (BufferedReader tcReader = new BufferedReader(new FileReader(tcFileName))) {
			String sentence = null;
			while ((sentence = tcReader.readLine()) != null) {
				String[] options = AnswerOptionsDetector.getOptions(sentence);
				String questionId = options[0];
				float max = -1.0f;
				int bestfit = -1;
				for (int i = 1; i < 5; i++) {
					BitSet bitSet = CorpusToBitSetMapper.getBitSetFromSentence(options[i], wordToClusterMap);
					float value = corpusDotProdCalc.calculateScore(bitSet);
					if (value > max) {
						bestfit = i;
						max = value;
					} else if (value == max && bestfit != -1) {
						logger.info("question_id : " + questionId + " option" + (i - 1) + " = " + " option" + i);
					}
				}
				sb.append(questionId).append(",").append(getOption(bestfit)).append("\n");
			}
		}
		logger.info("Answers" + sb.toString());
		FileUtil.writeAsString(sb, new FileOutputStream(answerFile));
		
	}
	private static String getOption(int i){
		char option = (char) ('A' + (i-1));
		return String.valueOf(option);
	}

}
