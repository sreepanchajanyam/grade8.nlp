package grade8.nlp.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.util.FileUtil;

public class SentenceDetectorMain {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		String[] sentences = SentenceDetector.getSentences(new FileInputStream(new File("/home/centos/Desktop/ck12.txt")));
		StringBuilder sb = new StringBuilder();
		for(String sentence : sentences) {
			sb.append(sentence).append("\n");
		}
		FileUtil.writeAsString(sb, new FileOutputStream(new File("/home/centos/Desktop/ck12_sentences.txt")));
	}

}
