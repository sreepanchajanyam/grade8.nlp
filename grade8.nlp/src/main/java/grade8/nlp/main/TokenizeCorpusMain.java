package grade8.nlp.main;

import grade8.nlp.executor.TokenizerExecutor;
import grade8.nlp.filter.StopWordFilterForTokens;
import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.filter.WordCleanserForTokens;
import grade8.nlp.util.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;


/**
 * 
 * Tokenizes an input file that 
 * keywords \t content
 * 
 * @author Sree Panchajanyam D
 * 
 */

public class TokenizeCorpusMain {

	private static Integer MIN_FREQUENCY = 5;
	private static Logger logger = Logger.getLogger(TokenizeCorpusMain.class);

	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
		
		String inputfile = args[0]; 
		String outputfile = args[1];
		 
		
		String hyperLinkRegex = "^(https|http|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		String hyperLinkRegexFirstPart = "://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	
		TokenFilter duplicateWordFilter = new WordCleanserForTokens();
		TokenFilter stopwordsFilter = new StopWordFilterForTokens(Arrays.asList(FileUtil.getString(FileUtil.getInputStream("models/en-stopwords")).split(" ")));
		Scanner scanner = new Scanner(new FileInputStream(inputfile));
		
		String sentence = null;
		ExecutorService executorService = Executors.newFixedThreadPool(20);
		List<Future<String[]>> futureList = new ArrayList<Future<String[]>>();
		long startTime = System.nanoTime();
		System.out.println("Started tokenizing...");
		while (scanner.hasNextLine()) {
			sentence = scanner.nextLine();
			sentence = sentence
							.replaceAll(hyperLinkRegex, "")
							.replaceAll(hyperLinkRegexFirstPart, "")
							.replaceAll("/", " ").trim();
			
							//.replaceAll(hyperLinkRegexSecondPart, "").trim();
			//logger.info(sentence);
			Future<String[]> future = executorService.submit(new TokenizerExecutor(sentence, duplicateWordFilter, stopwordsFilter));
			futureList.add(future);
		}
		Map<String,Integer> frequencyMap = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		for (Future<String[]> future : futureList) {
			String[] tokens = future.get();
			for (String token : tokens) {
				if (frequencyMap.containsKey(token) && frequencyMap.get(token)!=-1) {
					int count = frequencyMap.get(token)+1;
					if(count >= MIN_FREQUENCY) {
						sb.append(token).append("\n"); // add word to the stringbuilder
						frequencyMap.put(token, -1); // do not consider this word futher
					} else {
						frequencyMap.put(token, count);
					}
				}else if(!frequencyMap.containsKey(token)){
					frequencyMap.put(token, 1);
				}
			}
		}
		
		logger.info("Tokenizing Done, time taken complete executor = " + (System.nanoTime() -  startTime)/1000000000L);
		startTime = System.nanoTime();
		logger.info(outputfile);
		FileUtil.writeAsString(sb, new FileOutputStream(outputfile));
		logger.info("Tokenizing Done, time taken to write to file= " + (System.nanoTime() -  startTime)/1000000000L);
		executorService.shutdown();
	}

}
