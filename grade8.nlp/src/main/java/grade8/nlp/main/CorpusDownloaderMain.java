package grade8.nlp.main;

import grade8.nlp.download.ContentDownloader;
import grade8.nlp.download.DBPediaAbstractDownloader;
import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.service.CorpusDownloader;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class CorpusDownloaderMain {

	private final static Logger logger = Logger.getLogger(CorpusDownloaderMain.class);
	private final static String DEFAULT_OUTPUT_FILE = "corpus.out";

	public static void main(String[] args) throws Exception {
		String outputFile = DEFAULT_OUTPUT_FILE;
		String inputFile = null;
		
		if (args == null || args.length < 2) {
			logger.info("usage : java -jar <jar_name> input_filename output_filename");
			throw new Exception("Input File Name and Output File Name are expected ");
		}

		
		if (args[0] != null && !"".equals(args[0].trim())) {
			inputFile = args[0];
		} else {
			throw new Exception("Invalid Input File Name");
		}
		
		if (args[1] != null && !"".equals(args[1].trim())) {
			outputFile = args[1];
		}
		
		List<String> filterTagsList = new ArrayList<>();
		filterTagsList.add("NN");
		filterTagsList.add("NNS");
		filterTagsList.add("NNP");
		filterTagsList.add("NNPS");
		TokenFilter tokenFilter = new TagFilterForTokens(filterTagsList);
		Class<? extends ContentDownloader> downloaderClass = null;
		
		if("wikipedia".equalsIgnoreCase(args[2]))
			downloaderClass = (Class<? extends ContentDownloader>) Class.forName("WikiESContentDownloader");
		else
			downloaderClass = (Class<? extends ContentDownloader>) Class.forName("DBPediaAbstractDownloader");
		
		CorpusDownloader corpusDownloader = new CorpusDownloader(downloaderClass.newInstance());
		corpusDownloader.downloadCorpusForTokens(new FileInputStream(inputFile), new FileOutputStream(outputFile), tokenFilter);
	}
	
	
}
