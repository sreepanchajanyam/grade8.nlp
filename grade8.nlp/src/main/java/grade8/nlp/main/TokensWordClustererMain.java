package grade8.nlp.main;

import grade8.nlp.service.TokensWordClusterer;
import grade8.nlp.util.FileUtil;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.Map;

import org.apache.log4j.Logger;

public class TokensWordClustererMain {

	private final static Logger logger = Logger.getLogger(TokensWordClustererMain.class);
	private final static String DEFAULT_OUTPUT_FILE = "word2vec.out";

	public static void main(String[] args) throws Exception {
		String outputFile = DEFAULT_OUTPUT_FILE;
		String inputFile = null;
		if (args == null || args.length < 2) {
			logger.info("usage : java -jar <jar_name> input_filename output_filename -Dclustermode=<path_to_deps.words> [-Dnumclusters=int] [-Dnumtrials=int] [-Dnumiters=int]");
			throw new Exception("Input File Name and Output File Name are expected ");
		}
		
		if (args[0] != null && !"".equals(args[0].trim())) {
			inputFile = args[0];
		} else {
			logger.info("usage : java -jar <jar_name> input_filename output_filename -Dclustermode=<path_to_deps.words> [-Dnumclusters=int] [-Dnumtrials=int] [-Dnumiters=int]");
			throw new Exception("Corpus File Name");
		}
		if (args[1] != null && !"".equals(args[1].trim())) {
			outputFile = args[1];
		}
		long startime = System.nanoTime();
		Map<String, Integer> word2ClusterMap= TokensWordClusterer.getWordClusterMapFromTokens(new FileInputStream(inputFile));
		FileUtil.writeAsString((Serializable) word2ClusterMap,  new FileOutputStream(outputFile));
		FileUtil.writeAsObject((Serializable) word2ClusterMap,  new FileOutputStream(outputFile+".bin"));
		logger.info("Timetaken = " + (System.nanoTime() - startime)/1000000000);
	}
	

}
