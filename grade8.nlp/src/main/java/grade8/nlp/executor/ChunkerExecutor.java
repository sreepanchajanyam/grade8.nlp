package grade8.nlp.executor;

import grade8.nlp.core.Chunker;
import grade8.nlp.core.Tagger;
import grade8.nlp.core.Tokenizer;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

public class ChunkerExecutor extends NLPExecutor<String[]> {
	
	
	private final List<String> chunkTypes;
	private final String sentence;
	
	public ChunkerExecutor(final String sentence, final List<String> chunkTypes) {
		this.sentence = sentence;
		this.chunkTypes = chunkTypes;
	}
	
	@Override
	public String[] execute() throws Exception {
		Tokenizer tokenizer = new Tokenizer(sentence);
		String[] tokens = tokenizer.getTokens();
		Tagger tagger = new Tagger(tokens);
		String[] tags = tagger.getPOSTags();

		Chunker chunker = new Chunker(tokens, tags, chunkTypes);
		return chunker.getChunks();
	}
	
}
