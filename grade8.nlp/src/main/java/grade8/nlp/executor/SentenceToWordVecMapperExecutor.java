package grade8.nlp.executor;

import grade8.nlp.core.Tokenizer;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.word2vec.Word2VecConverter;
import grade8.nlp.word2vec.Word2VectorMap;

/**
 * Takes an sentence, breaks it into string tokens(words), applies filters if
 * any and, for each word generates a Vector using Word2Vec utility.
 * 
 * @author Sree Panchajanyam D
 * 
 */
public class SentenceToWordVecMapperExecutor extends NLPExecutor<Word2VectorMap[]> {

	private final TokenFilter[] tokenFilters;
	private final String sentence;

	public SentenceToWordVecMapperExecutor(String sentence, TokenFilter... tokenFilters) {
		this.sentence = sentence;
		this.tokenFilters = tokenFilters;
	}

	@Override
	public Word2VectorMap[] execute() throws Exception {
		Tokenizer tokenizer = new Tokenizer(sentence);
		String[] tokens = tokenizer.getTokens();
		if (tokenFilters != null) {
			for (TokenFilter tokenFilter : tokenFilters) {
				tokens = tokenFilter.filter(tokens);
			}
		}

		Word2VectorMap[] word2VecArr = new Word2VectorMap[tokens.length];
		for (int i = 0; i < tokens.length; i++) {
			double[] vector = Word2VecConverter.getWordVector(tokens[i]);
			word2VecArr[i] = new Word2VectorMap(tokens[i], vector);
		}

		return word2VecArr;
	}

}