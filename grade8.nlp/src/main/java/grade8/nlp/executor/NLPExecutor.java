package grade8.nlp.executor;

import java.util.concurrent.Callable;

public abstract class NLPExecutor<T> implements Callable<T> {
	/*
	protected final String sentence;
	
	public NLPExecutor(String sentence) {
		this.sentence = sentence;
	}*/
	
	@Override
	public T call() throws Exception {
		return execute();
	}

	public abstract T execute() throws Exception;

}
