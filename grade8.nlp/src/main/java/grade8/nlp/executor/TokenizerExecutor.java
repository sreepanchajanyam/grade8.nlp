package grade8.nlp.executor;

import grade8.nlp.core.Tokenizer;
import grade8.nlp.filter.TokenFilter;

public class TokenizerExecutor extends NLPExecutor<String[]> {

	private final TokenFilter[] tokenFilters;
	private final String sentence;

	public TokenizerExecutor(String sentence, TokenFilter... tokenFilters) {
		this.sentence = sentence;
		this.tokenFilters = tokenFilters;
	}

	public TokenizerExecutor(String sentence) {
		this(sentence, null);

	}

	@Override
	public String[] execute() throws Exception {
		Tokenizer tokenizer = new Tokenizer(sentence,tokenFilters);
		return tokenizer.getTokens();
	}

}
