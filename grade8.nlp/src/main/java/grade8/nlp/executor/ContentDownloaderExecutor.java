package grade8.nlp.executor;

import grade8.nlp.download.ContentDownloader;

public class ContentDownloaderExecutor extends NLPExecutor<String> {
	
	private final String title;
	private final ContentDownloader contentDownloader;
	
	public ContentDownloaderExecutor(String title, ContentDownloader contentDownloader) {
		this.title = title;
		this.contentDownloader = contentDownloader;
	}
	
	@Override
	public String execute() throws Exception {
		return contentDownloader.download(title);
	}

}
