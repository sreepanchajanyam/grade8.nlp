package grade8.nlp.core;

import grade8.nlp.consts.NLPConstants;
import grade8.nlp.util.FileUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.util.Span;

import org.apache.log4j.Logger;

public class Chunker {

	private static ChunkerModel chunkerModel = null;
	private static final Set<String> stopwords = new HashSet<>();
	private static final String WORD_DELIMITER = " ";
	
	private final String[] toks;
	private final String[] tags;
	private final List<String> chunkTypes;
	private final ChunkerME chunker;
	private final static Logger logger = Logger.getLogger(Chunker.class); 

	static {
		InputStream stopwordsModel = null;
		stopwordsModel = FileUtil.getInputStream(NLPConstants.STOPWORD_MODEL);
		String line = null;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(stopwordsModel))) {
			while ((line = br.readLine()) != null) {
				stopwords.add(line.toLowerCase());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (InputStream chunkerModelFile = FileUtil.getInputStream(NLPConstants.CHUNK_MODEL)) {
			chunkerModel = new ChunkerModel(chunkerModelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Constructor that takes in relevant parameter
	 * @param toks
	 * @param tags
	 * @param chunkTypes
	 */
	public Chunker(String[] tokens, String[] tags, List<String> chunkTypes) {
		this.toks = tokens;
		this.tags = tags;
		this.chunkTypes = chunkTypes;
		this.chunker = new ChunkerME(chunkerModel);
	}

	public String[] getChunks() {
		return groupSelectedChunks(getSpans());
	}
	
	
	/**
	 * group chunks of types as supplied by the user in List<String> chunkTypes.
	 * @param spans
	 * @return
	 */
	private String[] groupSelectedChunks(Span[] spans) {

		// if user is not looking for a specific chunkType return all chunks
		if (chunkTypes == null || chunkTypes.isEmpty()) return groupAllChunks(spans);

		List<String> groupedChunkList = new ArrayList<>();
		for (Span span : spans) {
			if (chunkTypes.contains(span.getType()) && getChunk(span) != null) groupedChunkList.add(getChunk(span));
		}
		if (logger.isTraceEnabled()) logger.trace(groupedChunkList);
		return groupedChunkList.toArray(new String[groupedChunkList.size()]);
	}

	private Span[] getSpans() {
		return chunker.chunkAsSpans(toks, tags);

	}

	/**
	 * transform a chunk represented as a span to string example: "He said" ---
	 * is a chunk
	 * 
	 * @param toks
	 * @param span
	 * @return
	 */
	private String getChunk(Span span) {

		if ((span.getEnd() - span.getStart() < 2) && stopwords.contains(toks[span.getStart()].toLowerCase())) return null;
		StringBuilder sb = new StringBuilder();
		for (int i = span.getStart(); i < span.getEnd(); i++)
			sb.append(toks[i]).append(WORD_DELIMITER);
		String str = sb.toString().replaceAll("[.?(),\"]", "");

		if ("".equals(str) || stopwords.contains(str)) return null;

		return sb.toString();
	}
	
	/**
	 * group all toks into chunks(represented as strings) and return an String
	 * Array of chunks
	 * 
	 * @param toks
	 * @param spans
	 * @return
	 */
	private String[] groupAllChunks(Span[] spans) {
		List<String> groupedChunkList = new ArrayList<>();
		for (Span span : spans) {
			if (getChunk(span) != null) groupedChunkList.add(getChunk(span));
		}
		if (logger.isTraceEnabled()) logger.trace(groupedChunkList);
		return groupedChunkList.toArray(new String[groupedChunkList.size()]);
	}

}
