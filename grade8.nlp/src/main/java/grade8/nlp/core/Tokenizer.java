package grade8.nlp.core;

import grade8.nlp.consts.NLPConstants;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.util.FileUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import org.apache.log4j.Logger;

public class Tokenizer {

	private static TokenizerModel tokenizerModel = null;
	private final Logger logger = Logger.getLogger(Tokenizer.class);
	private final TokenizerME tokenizer = new TokenizerME(tokenizerModel);

	static {
		try (InputStream tokenizerModelFile = FileUtil.getInputStream(NLPConstants.TOKEN_MODEL)) {
			tokenizerModel = new TokenizerModel(tokenizerModelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private final String sentence;
	private TokenFilter[] tokenFilters;

	public Tokenizer(String sentence) {
		this.sentence = sentence;
	}

	public Tokenizer(String sentence, TokenFilter... tokenFilters) {
		this.sentence = sentence.toLowerCase();
		this.tokenFilters = tokenFilters;
	}

	public String[] getTokens() throws Exception {
		String[] tokens = tokenizer.tokenize(sentence);
		if(logger.isTraceEnabled()) logger.trace(" Tokens BEFORE filtering : " + Arrays.toString(tokens));
		for (TokenFilter tokenFilter : tokenFilters) {
			if (tokens != null && tokens.length > 0) tokens = tokenFilter.filter(tokens);
		}
		if(logger.isTraceEnabled()) logger.trace(" Tokens AFTER filtering : " + Arrays.toString(tokens));
		return tokens;

	}

}
