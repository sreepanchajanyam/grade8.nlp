package grade8.nlp.core;

import grade8.nlp.consts.NLPConstants;
import grade8.nlp.util.FileUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

public class Tagger {


	private static  POSModel posModel = null;
	private POSTaggerME tagger = new POSTaggerME(posModel);
	
	static {
		try(InputStream POSTaggerModelFile = FileUtil.getInputStream(NLPConstants.POS_MODEL)) {
			posModel = new POSModel(POSTaggerModelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private final String[] tokens;
	
	public Tagger(String[] tokens) {
		this.tokens = tokens;
	}
	
	public  String[] getPOSTags() throws IOException {
		return tagger.tag(tokens);
	}

	

}
