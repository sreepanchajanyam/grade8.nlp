package grade8.nlp.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Using the grade8.nlp library
 * 1. Run CorpusDownloader on training_set.tsv and save to file corpus_download
 * 2. Run CorpusWordClusterer on file corpus_download and save the map to file word_cluster_map
 * 3. Run CorpusToBitSetMapper on files corpus_download , word_cluster_map to get WordToBitSetMap(Sentence, BitSet), 
 * 	  and save to file knowledge_base
 * 4. Download text file questions_set
 * 5. Run CorpusToBitSetMapper on all 4 options of each question of the questions_set file,
 *    and generate WordToBitSetMap(Option, BitSet), and save to file question_bank
 * 6. for every Question and corresponding 4 answers, get a Scalar Dot Product with BitSets in knowledge_base.
 * 7. Option with the highest Scalar Dot Product is the answer for a given question.  
 */

/* Method to parse unstructured data into Facts. */
public class FactAtoms {

	/** Fetch keywords from training data **/
	public List<String> getKeywords(String trainingData) {
		List<String> keywordList = new ArrayList<String>();
		// Iterate through sentences of given trainingData
		// get POS tags of each sentence
		// extract Nouns and Verbs
		return keywordList;
	}
	
	/** Fetch relevant unstructured docs from various sources**/
	// This one is for wiki
	// check: https://code.google.com/p/gwtwiki/wiki/Mediawiki2PlainText
	// check: http://www.evanjones.ca/software/wikipedia2text.html
	// check: http://stackoverflow.com/questions/9151075/jsoup-extract-text-from-wikipedia-article
	public String getDocsWiki(String keyword) {
		String document = new String();
		// hit the wiki search api
		// return content
		return document;
	}
	// Add dbpedia, yago etc. TODO
	
	/** Indexing to Elasticsearch **/
	// Pushes code to ES
	public void indexRelevantDocsIntoES(String Doc) {
		return;
	}
	// Or Write Doc to text file and use esfetch.py to push to ES
	public void writeRelevantDocsIntoFile(String Doc) {
		return;
	}
	
	/** Facts from ES **/
	/* Given a sentence it runs the chunker and gets all the
	 * different chunks mapped into our fact space.
	 * 
	 * TODO figure out fact space
	 */
	public List<String> getFactTriplets(String sentence) {
		List<String> sr = getKeywords(sentence);
		// Run opennlp chunker
		return null;
	}
	
	/* Dummy methods */
	public String getParsedSent(String rawSent) {
		String dummy = new String();
		return dummy;
	}
	
}
