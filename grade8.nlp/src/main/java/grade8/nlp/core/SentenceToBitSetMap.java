package grade8.nlp.core;

import java.io.Serializable;
import java.util.BitSet;

public class SentenceToBitSetMap implements Serializable{
	
	
	private static final long serialVersionUID = -5203629598813180297L;
	private transient final String sentence;
	private final BitSet bitSet;
	
	public SentenceToBitSetMap(String sentence, BitSet bitSet) {
		this.sentence = sentence;
		this.bitSet = bitSet;
	}

	public String getSentence() {
		return sentence;
	}

	public BitSet getBitSet() {
		return bitSet;
	}

	@Override
	public String toString() {
		return "SentenceToBitSetMap : sentence=" + sentence + ", bitSet=" + bitSet + "]";
	}
	
	
	
}
