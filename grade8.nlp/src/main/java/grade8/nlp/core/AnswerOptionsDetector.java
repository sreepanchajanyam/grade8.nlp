package grade8.nlp.core;

import java.io.IOException;

public class AnswerOptionsDetector {
	
	/**
	 * parse file that has is of the following format
	 * id \t OptionA \t OptionB \t OptionC \t OptionD
	 * @param sentence
	 * @return
	 * @throws IOException
	 */
	public static String[] getOptions(String sentence) throws IOException {
		return sentence.split("\t");

	}
}
