package grade8.nlp.core;

import grade8.nlp.consts.NLPConstants;
import grade8.nlp.util.FileUtil;

import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

import org.apache.log4j.Logger;

public class SentenceDetector {
	private static SentenceDetectorME sentenceDetector = null;
	private static final Logger logger = Logger.getLogger(SentenceDetector.class);

	/**
	 * resource intensive operation do it once during the time of class loading
	 */
	static {
		try {

			InputStream sentenceModelFile = FileUtil.getInputStream(NLPConstants.SENTENCE_MODEL);
			SentenceModel sentenceModel = new SentenceModel(sentenceModelFile);
			sentenceDetector = new SentenceDetectorME(sentenceModel);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String[] getSentences(InputStream inputStream) throws IOException {
		return sentenceDetector.sentDetect(FileUtil.getString(inputStream).toLowerCase());

	}
}
