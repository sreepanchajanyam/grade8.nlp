package grade8.nlp.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;

import org.apache.log4j.Logger;

public class FileUtil {
	private static final Logger logger = Logger.getLogger(FileUtil.class);

	public static String getString(InputStream inputStream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		while ((line = reader.readLine()) != null)
			stringBuilder.append(line).append(" ");
		return stringBuilder.toString();
	}

	public static InputStream getInputStream(String fileLocation) {
		return FileUtil.class.getClassLoader().getResourceAsStream(fileLocation);
	}

	public static void writeAsString(Object[] objArr, OutputStream fous) {
		try (PrintWriter printWriter = new PrintWriter(fous)) {
			for (Object obj : objArr) {
				writeAsString(obj, printWriter);
				printWriter.append("\n");
			}
			printWriter.flush();
		}
	}

	public static void writeAsString(Object obj, OutputStream fous) {
		try (PrintWriter printWriter = new PrintWriter(fous)) {
			writeAsString(obj, printWriter);
			printWriter.flush();
		}
	}
	
	private static void writeAsString(Object obj, PrintWriter printWriter) {
			printWriter.append(obj.toString());
	}

	public static Object readObject(InputStream inputStream) throws IOException, ClassNotFoundException {
		try (ObjectInputStream ois = new ObjectInputStream(inputStream)) {
			return ois.readObject();
		}
	}

	public static void writeAsObject(Serializable obj, OutputStream fout) throws IOException {
		try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
			oos.writeObject(obj);
		}
	}
	
	public static boolean doesFileExist(String fileName) {
		File file = new File(fileName);
		if (!file.exists() || !file.isFile()) return false;
		return true;
	}

}
