package grade8.nlp.util;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.download.DBPediaAbstractDownloader;
import grade8.nlp.filter.TagFilterForTokens;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.service.CorpusDownloader;
import grade8.nlp.service.CorpusToBitSetMapper;
import grade8.nlp.service.CorpusWordClusterer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class PropertyUtil {

	public static String getStringProperty(String key) throws Exception {
		String value = System.getProperty(key);
		if (value == null || "".equals(value)) throw new Exception("Invalid Key : " + key);
		return value;
	}
	
	public static String getStringProperty(String key, String defaultValue) {
		String value = defaultValue;
		try {
			value = getStringProperty(key);
		} catch (Exception e) {
		}
		return value;
	}

	public static int getIntProperty(String key, int defaultValue) {
		String strValue = System.getProperty(key);
		int intValue = defaultValue;
		try {
			if (strValue != null && !"".equals(strValue.trim())) intValue = Integer.parseInt(strValue);
		} catch (NumberFormatException nfe) {
		}
		return intValue;
	}


	
}
