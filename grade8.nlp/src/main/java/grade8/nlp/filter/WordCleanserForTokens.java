package grade8.nlp.filter;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * General Purpose Word Cleanser
 * Remove special characters
 * Remove duplicate words in a sentence
 * Remove words smaller than 3 letters
 * @author Sree Panchajanyam D
 *
 */
public class WordCleanserForTokens implements TokenFilter{

	private Integer allowedTokenSize = 3;
	private int minFrequency = 5;
	public WordCleanserForTokens() {
		// TODO Auto-generated constructor stub
	}
	
	public WordCleanserForTokens(int allowedTokenSize, int minFrequency) {
		this.allowedTokenSize = allowedTokenSize;
		this.minFrequency  = minFrequency;
	}
	
	@Override
	public String[] filter(String[] tokens) throws Exception {
		
		List<String> tokenList = new ArrayList<>();
		Map<String,Integer> frequencyMap = new HashMap<>();
	/*	for (String token : tokens) {
			token = token.replaceAll("[^A-Za-z].*", " ").trim();
			if (frequencyMap.containsKey(token) && frequencyMap.get(token)!=-1) {
				int count = frequencyMap.get(token)+1;
				if(count >= minFrequency) {
					tokenList.add(token); // add word to the stringbuilder
					frequencyMap.put(token, -1); // do not consider this word futher
				} else {
					frequencyMap.put(token, count);
				}
			}else if(!frequencyMap.containsKey(token)){
				frequencyMap.put(token, 1);
			}
		}*/
		Set<String> uniqueTokens = new HashSet<String>();
		for (String token : tokens) {
			token = token.replaceAll("[^A-Za-z].*", " ").trim(); // replace puctuation and series of special characters with a white space, trim whitespaces
			if (token!=null && !"".equals(token) && token.length()>allowedTokenSize && !uniqueTokens.contains(token)){
				uniqueTokens.add(token);
				tokenList.add(token);
			}
		}
		return tokenList.toArray(new String[tokenList.size()]);
	}

}
