package grade8.nlp.filter;

public interface TokenFilter {
	
	public String[] filter(String[] tokens) throws Exception;
	
}
