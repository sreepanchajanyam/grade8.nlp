package grade8.nlp.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Filters out stopwords from array of tokens
 * 
 * @author Sree Panchajanyam D
 *
 */
public class StopWordFilterForTokens implements TokenFilter{
	
	private final List<String> stopWords;
	
	public StopWordFilterForTokens(List<String> stopWords) {
		this.stopWords = stopWords;
	}
	
	@Override
	public String[] filter(String[] tokens) {
		
		if(stopWords==null || stopWords.isEmpty()) return tokens;
		List<String> filteredTokens = new ArrayList<>();
		for(String token : tokens){
			if(!stopWords.contains(token))	filteredTokens.add(token);
		}
		return filteredTokens.toArray(new String[filteredTokens.size()]);
	}

}
