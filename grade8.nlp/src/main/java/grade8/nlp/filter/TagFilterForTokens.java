package grade8.nlp.filter;

import grade8.nlp.core.Tagger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;


public class TagFilterForTokens implements TokenFilter {
	
	private final List<String> filterTagList;
	private final Logger logger = Logger.getLogger(TagFilterForTokens.class); 
	public TagFilterForTokens( List<String> filterTagList) {
		this.filterTagList = filterTagList;
	}
	
	/**
	 * Select only those tokens that have tags listed in the filterTagList
	 */
	@Override
	public String[] filter(String[] tokens) throws IOException{
		if(filterTagList == null || filterTagList.isEmpty()) return tokens;
		Tagger tagger = new Tagger(tokens);
		String[] tags = tagger.getPOSTags();
		if(logger.isDebugEnabled()) logger.debug("tokens to be tagged : " + Arrays.toString(tokens));
		List<String> tokenList = new ArrayList<>();
		for(int i=0; i < tags.length; i++) {
			if(filterTagList.contains(tags[i])) {
				tokenList.add(tokens[i]);
			}
		}
		return tokenList.toArray(new String[tokenList.size()]);
	}
}
