package grade8.nlp.word2vec;

import java.util.Arrays;

public class Word2VectorMap {
	
	private final String word;
	private final double[] vector;
	
	public Word2VectorMap(String word, double[] vector) {
		this.word = word;
		this.vector = vector;
	}

	public String getWord() {
		return word;
	}

	public double[] getLocation() {
		return vector;
	}

	@Override
	public String toString() {
		return word + "=" + Arrays.toString(vector);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Word2VectorMap other = (Word2VectorMap) obj;
		if (word == null) {
			if (other.word != null) return false;
		} else if (!word.equals(other.word)) return false;
		return true;
	}
	
	
	
}
