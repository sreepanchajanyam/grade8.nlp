package grade8.nlp.word2vec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.clustering.MultiKMeansPlusPlusClusterer;
import org.apache.log4j.Logger;

/**
 * Purpose:
 * Clusters Words(String) based on the WordVectors(double[])
 * 
 * Description:
 * Core class that interacts with Apache KMeans Algorithm. 
 * KMeans Algorithm is a clustering algorithm thats clusters Words(String) based on the WordVectors(double[])
 * This class MultiKMeans Algo (a wrapper on KMeans) which runs clustering Algo NUM_TRIALS times and returns the 
 * best clustering result.
 * 
 * @author Sree Panchajanyam D
 *
 */

public class WordClusterer {

	private static int NUM_CLUSTERS ;
	private static int NUM_ITERATIONS;
	private static int NUM_TRIALS;

	static {
		NUM_CLUSTERS = getIntProperty("numclusters", 100);
		NUM_ITERATIONS = getIntProperty("numiters", 1000);
		NUM_TRIALS = getIntProperty("numtrials", 10);
	}
	
	private final static Logger logger = Logger.getLogger(WordClusterer.class);
	
	public Map<String,Integer> clusterWords(Word2VectorMap[] word2vecArr) {
		List<Word2VectorMapWrapper> clusterInput = new ArrayList<Word2VectorMapWrapper>(word2vecArr.length);
		for (Word2VectorMap word2Vec : word2vecArr)
			clusterInput.add(new Word2VectorMapWrapper(word2Vec));
		logger.info("Clustering  " + word2vecArr.length + " WordVectors, STARTED");
		KMeansPlusPlusClusterer<Word2VectorMapWrapper> kmeansClusterer = new KMeansPlusPlusClusterer<Word2VectorMapWrapper>(NUM_CLUSTERS, NUM_ITERATIONS);
		MultiKMeansPlusPlusClusterer<Word2VectorMapWrapper> clusterer = new MultiKMeansPlusPlusClusterer<Word2VectorMapWrapper>(kmeansClusterer, NUM_TRIALS);
		List<CentroidCluster<Word2VectorMapWrapper>> clusterResults = clusterer.cluster(clusterInput);
		if(logger.isTraceEnabled()) logger.trace("*************************NUM_CLUSTERS = " + NUM_CLUSTERS + " NUM_ITERATIONS = " + NUM_ITERATIONS + " NUM_TRIALS = " + NUM_TRIALS + "*********************************");
		
		Map<String,Integer> word2ClusterMap = new HashMap<String, Integer>();
		for (int i = 0; i < clusterResults.size(); i++) {
			if(logger.isTraceEnabled()) logger.trace("Cluster " + i);
			for (Word2VectorMapWrapper word2VectorMapWrapper : clusterResults.get(i).getPoints()) {
				if(logger.isTraceEnabled()) logger.trace(word2VectorMapWrapper.getWord2VectorMap().getWord() + "\t");
				word2ClusterMap.put(word2VectorMapWrapper.getWord2VectorMap().getWord(), i);
			}

			if(logger.isTraceEnabled()) logger.trace("\n");
		}
		logger.info("Clustering  " + word2vecArr.length + " WordVectors, COMPLETED");
		return word2ClusterMap;
	}

	private static int getIntProperty(String key, int defaultValue) {
		String strValue = System.getProperty(key);
		int intValue = defaultValue;
		try {
			if (strValue != null && !"".equals(strValue.trim()))  intValue = Integer.parseInt(strValue);
		} catch (NumberFormatException nfe) {
			if(logger.isTraceEnabled()) logger.trace("Value of " + key + " is not a number = " + strValue + "defaulting to : " + defaultValue);
		}
		return intValue;
	}
}
