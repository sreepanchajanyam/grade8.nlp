package grade8.nlp.word2vec;

import org.apache.commons.math3.ml.clustering.Clusterable;

public class Word2VectorMapWrapper implements Clusterable {
    
	private Word2VectorMap word2VectorMap;

    public Word2VectorMapWrapper(Word2VectorMap word2VectorMap) {
        this.word2VectorMap = word2VectorMap;
    }

    
    public double[] getPoint() {
        return word2VectorMap.getLocation();
    }

	public Word2VectorMap getWord2VectorMap() {
		return word2VectorMap;
	}
	
	
} 