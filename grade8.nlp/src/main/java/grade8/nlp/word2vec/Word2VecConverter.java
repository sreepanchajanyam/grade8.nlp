package grade8.nlp.word2vec;

import java.io.File;
import java.io.IOException;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;


/**
 * Core class that interacts with WordVectorSerializer to convert a Word to Vector
 * 
 * @author Sree Panchajanyam D
 *
 */
public class Word2VecConverter {

	private static File gModel;

	private static WordVectors wordVectors = null;

	static {
		try {
			String clusterModel = System.getProperty("clustermodel");
			gModel = new File(clusterModel);
			wordVectors = WordVectorSerializer.loadGoogleModel(gModel, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static double[] getWordVector(String token) throws IOException {
		return wordVectors.getWordVector(token);
	}
}