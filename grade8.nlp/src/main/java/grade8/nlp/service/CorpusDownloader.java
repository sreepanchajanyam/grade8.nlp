package grade8.nlp.service;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.download.ContentDownloader;
import grade8.nlp.executor.ContentDownloaderExecutor;
import grade8.nlp.executor.TokenizerExecutor;
import grade8.nlp.filter.TokenFilter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

/**
 * Class that downloads corpus for word tokens in a given file
 * 
 * @author Sree Panchajanyam D
 * 
 */
public class CorpusDownloader {

	private final static Logger logger = Logger.getLogger(CorpusDownloader.class);
	private final ContentDownloader contentDownloader;

	public CorpusDownloader(ContentDownloader contentDownloader) {
		this.contentDownloader = contentDownloader;
	}

	/**
	 * Parses inputStream into a list of string tokens. For each token Corpus is
	 * downloaded from respective websites like DBPedia, WikiPedia etc..
	 * 
	 * @param inputStream
	 * @param outputStream
	 * @param neededsTagList
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public void downloadCorpusForTokens(InputStream inputStream, OutputStream outputStream, TokenFilter tokenFilter) throws IOException, InterruptedException, ExecutionException {
		String[] sentences = SentenceDetector.getSentences(inputStream);
		downloadCorpusForTokens(sentences, outputStream, tokenFilter);
	}

	public void downloadCorpusForTokens(String[] sentences, OutputStream outputStream, TokenFilter tokenFilter) throws IOException, InterruptedException, ExecutionException {
		String[] tokens = getTokens(sentences, tokenFilter);
		saveCorpusToFile(tokens, outputStream);
	}

	private String[] getTokens(InputStream inputStream, TokenFilter tokenFilter) throws IOException, InterruptedException, ExecutionException {
		String[] sentences = SentenceDetector.getSentences(inputStream);
		return getTokens(sentences, tokenFilter);
	}

	private String[] getTokens(String[] sentences, TokenFilter tokenFilter) throws IOException, InterruptedException, ExecutionException {

		List<String> allTokens = new ArrayList<>();
		ExecutorService executor = Executors.newFixedThreadPool(20);
		Future<String[]>[] futureArr = new Future[sentences.length];
		for (int i = 0; i < sentences.length; i++) {
			Future<String[]> future = executor.submit(new TokenizerExecutor(sentences[i], tokenFilter));
			futureArr[i] = future;
		}

		for (Future<String[]> future : futureArr) {
			String[] chunks = future.get();
			allTokens.addAll(Arrays.asList(chunks));
		}
		if (logger.isTraceEnabled()) logger.trace("All Tokens:" + allTokens);
		executor.shutdown();

		return allTokens.toArray(new String[allTokens.size()]);
	}

	private void saveCorpusToFile(String[] tokens, OutputStream outputStream) throws IOException, InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(20);
		Future<String>[] futureArr = new Future[tokens.length];
		for (int i = 0; i < tokens.length; i++) {
			String tempChunk = tokens[i].substring(0, 1).toUpperCase() + tokens[i].substring(1, tokens[i].length());
			futureArr[i] = executor.submit(new ContentDownloaderExecutor(tempChunk, contentDownloader));
		}

		try (PrintStream printStream = new PrintStream(outputStream)) {
			
			printStream.append("id");
			printStream.append("\t");
			printStream.append("token");
			printStream.append("\t");
			printStream.append("content");
			printStream.append("\n");
			for (int i= 0; i < futureArr.length; i++) {
				String content = futureArr[i].get();
				if (content != null) {
					printStream.append(""+i);
					printStream.append("\t");
					printStream.append(tokens[i]);
					printStream.append("\t");
					printStream.append(content);
					printStream.append("\n");
				}
			}
			printStream.flush();
		}

		executor.shutdown();
	}

}
