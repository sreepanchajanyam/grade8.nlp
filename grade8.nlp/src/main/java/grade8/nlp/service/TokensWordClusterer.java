package grade8.nlp.service;

import grade8.nlp.util.FileUtil;
import grade8.nlp.word2vec.Word2VecConverter;
import grade8.nlp.word2vec.Word2VectorMap;
import grade8.nlp.word2vec.WordClusterer;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;

public class TokensWordClusterer {
	
	
	
	private final static Logger logger = Logger.getLogger(TokensWordClusterer.class);
	
	public static Map<String, Integer> getWordClusterMapFromTokens(InputStream inputStream) throws IOException, InterruptedException, ExecutionException {
		String[] tokens = FileUtil.getString(inputStream).split(" ");
		logger.info("Identified  " + tokens.length + " words from InputStream");
		return getWordClusterMapFromTokens(tokens);
	}
	
	
	public static Map<String, Integer> getWordClusterMapFromTokens(String[] tokens) throws IOException, InterruptedException, ExecutionException {
		Word2VectorMap[] word2vecMapArr = getWordVectorMapFromTokens(tokens);
		Map<String, Integer> word2ClusterMap = clusterWordVectorsToMap(word2vecMapArr);
		if (logger.isTraceEnabled()) logger.trace(word2ClusterMap);
		return word2ClusterMap;
	}
	
	private static Map<String, Integer> clusterWordVectorsToMap(Word2VectorMap[] word2vecMapArr) {
		WordClusterer wordClusterer = new WordClusterer();
		Map<String, Integer> word2ClusterMap = wordClusterer.clusterWords(word2vecMapArr);
		return word2ClusterMap;
	}
	
	public static Word2VectorMap[] getWordVectorMapFromTokens(String[] tokens) throws InterruptedException, ExecutionException, IOException {
		Word2VectorMap[] word2VecMapArr = new Word2VectorMap[tokens.length];
		logger.info("Generation of Vectors for " + tokens.length + " words, STARTED");
		for (int i = 0; i < tokens.length; i++) {
			double[] vector = Word2VecConverter.getWordVector(tokens[i]);
			word2VecMapArr[i] = new Word2VectorMap(tokens[i], vector);
		}
		logger.info("Generation of Vectors for " + tokens.length + " words, COMPLETED");
		return word2VecMapArr;
	}

}
