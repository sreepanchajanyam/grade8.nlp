package grade8.nlp.service;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.util.FileUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.BitSet;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * This class operates on a Corpus (group of sentences) and, for each sentence
 * generates a BitSet. For each sentence in the corpus, the public method in the
 * corpus returns a SentenceToBitSetMap.
 * 
 * 
 * @author Sree Panchajanyam D
 * 
 */
public class CorpusToBitSetMapper {

	private static Logger logger = Logger.getLogger(CorpusToBitSetMapper.class);

	
	/**
	 * Output of this method is an array of SentenceToBitSetMap[], one array
	 * entry for each sentence
	 * 
	 * @param sentenceCorpusInputStream
	 * @param wordToClusterInputStream
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	

	public static SentenceToBitSetMap[] getBitSetArray(InputStream sentenceCorpusInputStream, InputStream wordClusterMapInputStream) throws IOException, ClassNotFoundException {
		Map<String, Integer> wordToClusterMap = (Map<String, Integer>) FileUtil.readObject(wordClusterMapInputStream);
		return getBitSetArray(sentenceCorpusInputStream, wordToClusterMap);
	}

	public static SentenceToBitSetMap[] getBitSetArray(InputStream sentenceCorpusInputStream, Map<String, Integer> wordToClusterMap) throws IOException, ClassNotFoundException {

		String[] sentences = SentenceDetector.getSentences(sentenceCorpusInputStream);
		SentenceToBitSetMap[] sentenceToBitSetMapArr = new SentenceToBitSetMap[sentences.length];
		for (int i = 0; i < sentences.length; i++) {

			BitSet bitSet = getBitSetFromSentence(sentences[i], wordToClusterMap);
			sentenceToBitSetMapArr[i] = new SentenceToBitSetMap(sentences[i], bitSet);
		}
		return sentenceToBitSetMapArr;
	}

	public static SentenceToBitSetMap[] getBitSetArray(String[] sentences, Map<String, Integer> wordToClusterMap) throws IOException, ClassNotFoundException {
		
		if(sentences==null) return null;
		
		SentenceToBitSetMap[] sentenceToBitSetMapArr = new SentenceToBitSetMap[sentences.length];
		for (int i = 0; i < sentences.length; i++) {

			BitSet bitSet = getBitSetFromSentence(sentences[i], wordToClusterMap);
			sentenceToBitSetMapArr[i] = new SentenceToBitSetMap(sentences[i], bitSet);
		}
		return sentenceToBitSetMapArr;
	}

	public static BitSet getBitSetFromSentence(String sentence, Map<String, Integer> wordToClusterMap) {
		BitSet bitSet = new BitSet();
		if(sentence ==null) return bitSet;
		String[] tokens = sentence.split(" ");
		for (String token : tokens) {
			Integer index = null;
			if ((index = wordToClusterMap.get(token)) != null) bitSet.set(index);
		}
		return bitSet;
	}
	
}
