package grade8.nlp.service;

import grade8.nlp.core.SentenceDetector;
import grade8.nlp.executor.SentenceToWordVecMapperExecutor;
import grade8.nlp.filter.TokenFilter;
import grade8.nlp.word2vec.Word2VecConverter;
import grade8.nlp.word2vec.Word2VectorMap;
import grade8.nlp.word2vec.WordClusterer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

/**
 * 
 * @author Sree Panchajanyam D This is a utility service for grouping words into
 *         clusters. After generating the clusters, a HashMap<String, Integer>
 *         -> (Word vs Cluster_Number) would be serialized to a file using the
 *         provided outputStream.
 * 
 *         Runs only on Ubuntu 14.0.4 due to JBlas native dependencies
 */
public class CorpusWordClusterer {

	private final static Logger logger = Logger.getLogger(CorpusWordClusterer.class);
	
	
	public static Map<String, Integer> getWordClusterMapFromCorpus(InputStream inputStream, TokenFilter tokenFilter) throws IOException, InterruptedException, ExecutionException {
		String[] sentences = SentenceDetector.getSentences(inputStream);
		return getWordClusterMapFromCorpus(sentences, tokenFilter);
	}
	
	
	
	public static Map<String, Integer> getWordClusterMapFromCorpus(String[] sentences, TokenFilter tokenFilter) throws IOException, InterruptedException, ExecutionException {
		Word2VectorMap[] word2vecMapArr = getWordVectorMap(sentences, tokenFilter);
		Map<String, Integer> word2ClusterMap = clusterWordVectorsToMap(word2vecMapArr);
		if (logger.isTraceEnabled()) logger.trace(word2ClusterMap);
		return word2ClusterMap;
	}
	
	
	
	

	private static Map<String, Integer> clusterWordVectorsToMap(Word2VectorMap[] word2vecMapArr) {
		WordClusterer wordClusterer = new WordClusterer();
		Map<String, Integer> word2ClusterMap = wordClusterer.clusterWords(word2vecMapArr);
		return word2ClusterMap;
	}

	public static Word2VectorMap[] getWordVectorMap(InputStream inputStream, TokenFilter tokenFilter) throws InterruptedException, ExecutionException, IOException {
		String[] sentences = SentenceDetector.getSentences(inputStream);
		return getWordVectorMap(sentences, tokenFilter);
	}

	public static Word2VectorMap[] getWordVectorMap(String[] sentences, TokenFilter tokenFilter) throws InterruptedException, ExecutionException, IOException {
		Set<Word2VectorMap> word2vecList = new HashSet<>();
		ExecutorService executor = Executors.newFixedThreadPool(20);

		Future<Word2VectorMap[]>[] futureList = new Future[sentences.length];
		for (int i = 0; i < sentences.length; i++) {
			Future<Word2VectorMap[]> future = executor.submit(new SentenceToWordVecMapperExecutor(sentences[i], tokenFilter));
			futureList[i] = future;

		}
		for (Future<Word2VectorMap[]> future : futureList) {
			word2vecList.addAll(Arrays.asList(future.get()));
		}

		executor.shutdown();
		return word2vecList.toArray(new Word2VectorMap[word2vecList.size()]);
	}
	
	
	
}
