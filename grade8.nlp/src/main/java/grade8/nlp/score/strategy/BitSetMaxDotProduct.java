package grade8.nlp.score.strategy;

import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.util.FileUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.BitSet;

import org.apache.log4j.Logger;

/**
 * 
 * @author Sree Panchajanyam D
 *
 */
public class BitSetMaxDotProduct implements ScoringStrategy<BitSet>{
	
	private final SentenceToBitSetMap[] corpusBitSetArr; 
	private final Logger logger = Logger.getLogger(BitSetMaxDotProduct.class);
	
	private String questionID = "";
	private String optionID = "";
	
	public BitSetMaxDotProduct(InputStream corpusBitSetStream,float[] esScores) throws ClassNotFoundException, IOException {
		this((SentenceToBitSetMap[]) FileUtil.readObject(corpusBitSetStream));
	}
	
	public BitSetMaxDotProduct(SentenceToBitSetMap[] corpusBitSetArr) throws ClassNotFoundException, IOException {
		this.corpusBitSetArr = corpusBitSetArr;
	}
	
	public float calculateScore(BitSet bitSet){
		BitSet tmpBitSet = null;
		float maxSize = 0;
		for(int i = 0; i < corpusBitSetArr.length; i++){
			tmpBitSet = (BitSet) bitSet.clone();
			tmpBitSet.and(corpusBitSetArr[i].getBitSet());
			if(tmpBitSet.cardinality() > maxSize) {
				maxSize = tmpBitSet.cardinality();
			}
		}
		return maxSize;
	}
	
	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	public void setOptionID(String optionID) {
		this.optionID = optionID;
	}
	
}
