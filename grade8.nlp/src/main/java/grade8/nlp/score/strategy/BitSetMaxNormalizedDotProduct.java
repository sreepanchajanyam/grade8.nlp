package grade8.nlp.score.strategy;

import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.util.FileUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.BitSet;

import org.apache.log4j.Logger;

/**
 * 
 * @author Sree Panchajanyam D
 *
 */
public class BitSetMaxNormalizedDotProduct implements ScoringStrategy<BitSet>{
	
	private final SentenceToBitSetMap[] corpusBitSetArr; 
	private final Logger logger = Logger.getLogger(BitSetMaxNormalizedDotProduct.class);
	
	private String questionID = "";
	private String optionID = "";
	
	public BitSetMaxNormalizedDotProduct(InputStream corpusBitSetStream) throws ClassNotFoundException, IOException {
		this((SentenceToBitSetMap[]) FileUtil.readObject(corpusBitSetStream));
	}
	
	public BitSetMaxNormalizedDotProduct(SentenceToBitSetMap[] corpusBitSetArr) throws ClassNotFoundException, IOException {
		this.corpusBitSetArr = corpusBitSetArr;
	}
	/**
	 * BitSet of OptionA = A
	 * BitSet[] of KnowledgeBase = B[]
	 * for each BitSet in KnowledgeBase calculate
	 * 	value = (A * B[i] )/ A.length *B[i].length
	 * return max(value)
	 *  	 
	 */
	public float calculateScore(BitSet bitSet){
		float aNormalizer = bitSet.cardinality(); // cardinality is number of set bits
		if(bitSet==null || corpusBitSetArr==null || aNormalizer==0) return 0.0f;
		BitSet tmpBitSet = null;
		float maxValue = 0.0f;
		
		float normalizer = 0.0f;
		for(int i = 0; i < corpusBitSetArr.length; i++){
			float bNormalizer = corpusBitSetArr[i].getBitSet().cardinality();
			tmpBitSet = (BitSet) bitSet.clone();
			tmpBitSet.and(corpusBitSetArr[i].getBitSet());
			normalizer = aNormalizer * bNormalizer;  
			float tmpValue = 0.0f;
			if(bNormalizer!=0) tmpValue = ((float) tmpBitSet.cardinality())/normalizer;
			
			if(tmpValue > maxValue) {
				maxValue = tmpValue;
			}
		}
		logger.info("maxValue: " + maxValue);
		return maxValue;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	public void setOptionID(String optionID) {
		this.optionID = optionID;
	}
}
