package grade8.nlp.score.strategy;

import grade8.nlp.core.SentenceToBitSetMap;
import grade8.nlp.service.CorpusToBitSetMapper;
import grade8.nlp.util.FileUtil;
import grade8.nlp.util.PropertyUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Map;

import org.apache.log4j.Logger;

public class ESnBitSetScoringStrategy implements ScoringStrategy<BitSet> {

	
	public static final String MAX_STRATEGY = "MAX";
	public static final String AVG_STRATEGY = "AVG";

	private final SentenceToBitSetMap[] corpusBitSetArr;
	private final String[] candidateSentences;
	private final float[] esScores;
	private final Logger logger = Logger.getLogger(ESnBitSetScoringStrategy.class);
	private final static String WORD_CLUSTER_MAP_FILE = "wordclustermapfile.out";
	private static Map<String, Integer> wordToClusterMap=null;
	private String questionID = "";
	private String optionID = "";
	private String strategy;
	
	static{
		String wordClusterMapFileName = PropertyUtil.getStringProperty(WORD_CLUSTER_MAP_FILE,"/home/centos/git/grade8.nlp/grade8.nlp/src/main/resources/models/clean_words_clustermap_1200.bin");
		try {
			wordToClusterMap = (Map<String, Integer>) FileUtil.readObject(new FileInputStream(wordClusterMapFileName));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	public ESnBitSetScoringStrategy(String[] candidateSentences, float[] esScores, String strategy)
			throws ClassNotFoundException, IOException {
		this.candidateSentences = candidateSentences;
		this.corpusBitSetArr = CorpusToBitSetMapper.getBitSetArray(candidateSentences, wordToClusterMap);	
		this.esScores = esScores;
		this.strategy = strategy;
	}

	@Override
	public float calculateScore(BitSet bitSet) {
		BitSet tmpBitSet = null;
		float optionBitSetSize = bitSet.cardinality(); // cardinality is number of
													// set bits
		if (bitSet == null || corpusBitSetArr == null) return 0.0f;
		ESnBitSetScoringVariables[] scoreVariableModelArr = new ESnBitSetScoringVariables[corpusBitSetArr.length];
		for (int i = 0; i < corpusBitSetArr.length; i++) {
			float candidateBitSetSize = corpusBitSetArr[i].getBitSet().cardinality();
			tmpBitSet = (BitSet) bitSet.clone();
			tmpBitSet.and(corpusBitSetArr[i].getBitSet());
			scoreVariableModelArr[i] = new ESnBitSetScoringVariables(esScores[i], optionBitSetSize,
					candidateBitSetSize, candidateSentences[i].length(), tmpBitSet.cardinality());
		}
		
		switch (this.strategy) {
		case MAX_STRATEGY:
			return max(scoreVariableModelArr);
		case AVG_STRATEGY:
			return avg(scoreVariableModelArr);
		default:
			return avg(scoreVariableModelArr);
		}
		
	}
	
	
	/**
	 * Scoring logic that includes all the following variables 
	 * esScore
	 * optionBitSetSize, 
	 * candidateBitSetSize
	 * candidateSentenceLength
	 * optionAndCandidateBitSetSize
	 */
	private float max(ESnBitSetScoringVariables[] scoreVariableModelArr) {
		float maxValue = 0.0f;
		for (ESnBitSetScoringVariables scoreVariables : scoreVariableModelArr) {
			float esRelevancy = scoreVariables.esScore;
			float candidateSentenceRelevancy = scoreVariables.candidateBitSetSize/scoreVariables.candidateSentenceLength;
			float optionRelevancy = scoreVariables.optionAndCandidateBitSetSize/scoreVariables.optionBitSetSize;
			float tmpValue = (esRelevancy + candidateSentenceRelevancy) * optionRelevancy;
			if(tmpValue > maxValue)  maxValue = tmpValue;
			if (logger.isInfoEnabled()) {
				logger.info(scoreVariables);
			}
			
		}
		return maxValue;
	}
	
	
	private float avg(ESnBitSetScoringVariables[] scoreVariableModelArr) {
		float cumulativeValue = 0.0f;
		for (ESnBitSetScoringVariables scoreVariables : scoreVariableModelArr) {
			float esRelevancy = scoreVariables.esScore;
			float candidateSentenceRelevancy = scoreVariables.candidateBitSetSize/scoreVariables.candidateSentenceLength;
			float optionRelevancy = scoreVariables.optionAndCandidateBitSetSize/scoreVariables.optionBitSetSize;
			float tmpValue = (esRelevancy + candidateSentenceRelevancy) * optionRelevancy;
			cumulativeValue = tmpValue + cumulativeValue;
			if (logger.isInfoEnabled()) {
				logger.info(scoreVariables);
			}
			
		}
		return cumulativeValue/scoreVariableModelArr.length;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	public void setOptionID(String optionID) {
		this.optionID = optionID;
	}

	public class ESnBitSetScoringVariables {

		private final float esScore;
		private final float optionBitSetSize;
		private final float candidateBitSetSize;
		private final float candidateSentenceLength;
		private final float optionAndCandidateBitSetSize;

		public ESnBitSetScoringVariables(float esScore, float optionBitSetSize,
				float candidateBitSetSize, float candidateSentenceLength, float optionAndCandidateBitSetSize) {
			this.esScore = esScore;
			this.optionBitSetSize = optionBitSetSize;
			this.candidateBitSetSize = candidateBitSetSize;
			this.candidateSentenceLength = candidateSentenceLength;
			this.optionAndCandidateBitSetSize = optionAndCandidateBitSetSize;
		}

		public String getHeaders() {
			StringBuilder sb = new StringBuilder();
			sb.append("QuestionID").append("\t").append("OptionID").append("\t").append("esScore")
					.append("\t").append("optionBitSetSize").append("\t")
					.append("candidateBitSetSize").append("\t")
					.append("candidateSentenceLength").append("\t")
					.append("optionAndCandidateBitSetSize");
			return sb.toString();

		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(questionID).append("\t").append(optionID).append("\t").append(esScore)
					.append("\t").append(optionBitSetSize).append("\t")
					.append(candidateBitSetSize).append("\t")
					.append(candidateSentenceLength).append("\t")
					.append(optionAndCandidateBitSetSize);
			return sb.toString();
		}

		public float getEsScore() {
			return esScore;
		}

		public float getOptionBitSetSize() {
			return optionBitSetSize;
		}

		public float getCandidateBitSetSize() {
			return candidateBitSetSize;
		}

		public float getOptionAndCandidateBitSetSize() {
			return optionAndCandidateBitSetSize;
		}
	}

}
