package grade8.nlp.score.strategy;

public interface ScoringStrategy<T> {
	
	public float calculateScore(T input);
	
	/**
	 * for logging purpose
	 * @param questionID
	 */
	public void setQuestionID(String questionID);
	
	/**
	 * for logging purpose
	 * @param optionID
	 */
	public void setOptionID(String optionID);

}
