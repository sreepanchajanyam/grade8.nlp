package grade8.nlp.consts;

public enum ChunkTypes {
    NOUN_PHRASE  ("NP"),  VERB_PHRASE("VP") ; 


    private final String value;

    ChunkTypes(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return this.value;
    }
    
}
