package grade8.nlp.consts;

public interface NLPConstants {

	public static final String SENTENCE_MODEL = "models/en-sent.bin";
	public static final String TOKEN_MODEL = "models/en-token.bin";
	public static final String POS_MODEL = "models/en-pos-maxent.bin";
	public static final String CHUNK_MODEL = "models/en-chunker.bin";
	public static final String STOPWORD_MODEL = "models/en-stopwords";
}
