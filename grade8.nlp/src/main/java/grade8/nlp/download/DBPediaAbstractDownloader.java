package grade8.nlp.download;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;


/**
 * 
 * Downloads abstract from DBPedia for a given string token
 *
 */
public class DBPediaAbstractDownloader implements ContentDownloader {
	

	private static final String dbpediaURLTemplate = "http://dbpedia.org/resource/{REPLACEABLE_TITLE}";
	private static final String TITLE_REPLACEABLE = "{REPLACEABLE_TITLE}";
	private static final String abstractContent = "li > span >span[property=dbo:abstract][xml:lang=en]";
	private static final Logger logger = Logger.getLogger(DBPediaAbstractDownloader.class);

		
	public String download(String title) throws IOException {
		String url = dbpediaURLTemplate.replace(TITLE_REPLACEABLE, title) ;
		String cleanContent = "";
		//System.out.println("[DBG] " + url);
		try {
			Document doc = null;
			doc = Jsoup.connect(url).get();
			Element contentDiv = doc.select(abstractContent).first();
	    	cleanContent = Jsoup.clean(contentDiv.toString(), new Whitelist());
		} catch (Exception e){
			String errorMessage = "[WARNING] Failed fetching content from url= [" + url + "] skipping. Error:" + e.toString();
			logger.warn(errorMessage);
		}
			if(logger.isTraceEnabled()) logger.trace(cleanContent);
		return cleanContent;
	}
}
