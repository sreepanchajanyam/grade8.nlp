package grade8.nlp.download;

import java.net.InetSocketAddress;
import java.util.Map;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

public class WikiESContentDownloader implements ContentDownloader {

	private static Client client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress("10.1.91.244", 9300)));
	private int limit = 50;
	private SearchRequestBuilder srb;
	private Logger logger = Logger.getLogger(WikiESContentDownloader.class);

	public WikiESContentDownloader(String[] indices, String type, Integer limit) {
		StringBuilder sb = new StringBuilder();
		if (indices == null) throw new RuntimeException("Indices cannot be null");
		if (type == null) throw new RuntimeException("type cannot be null");
		for (int i = 0; i < indices.length - 1; i++) {
			sb.append(indices[i]).append(",");
		}
		sb.append(indices[indices.length - 1]);
		sb.toString();
		if (limit != null && limit > 0) this.limit = limit;
		this.srb = client.prepareSearch(indices).setTypes(type).setSearchType(SearchType.QUERY_AND_FETCH).setSize(limit);
	}

	@Override
	public String download(String value) {
		return getDocuments(value).getContent();
	}

	public CorpusDoc getDocuments(String value) {

		String[] contentArr = null;
		float[] scoreArr = null;
		try {

			SearchResponse response = srb.setQuery(QueryBuilders.queryStringQuery(value)).execute().actionGet();
			SearchHit[] results = response.getHits().getHits();
			int nonEmptyCount = 0;
			contentArr = new String[limit];
			scoreArr = new float[limit];
			for (SearchHit hit : results) {
				if (nonEmptyCount >= limit) break;
				Map<String, Object> result = hit.getSource();
				String content = (String) result.get("content");
				if (content == null || "".equals(content.trim())) continue;
				scoreArr[nonEmptyCount] = hit.getScore();
				contentArr[nonEmptyCount] = content;
				nonEmptyCount++;
			}
		} catch (Exception e) {
			logger.error("Exception while downloading content from wiki for search string : " + value);
		}
		return new CorpusDoc(contentArr, scoreArr);
	}

}
