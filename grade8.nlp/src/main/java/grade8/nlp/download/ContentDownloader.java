package grade8.nlp.download;

import java.io.IOException;

/**
 * 
 * Interface to implement a downloader from variety  of sources like DBPedia, Wikipedia etc..
 *
 */
public interface ContentDownloader {
	
	public String download(String title) throws IOException;

}
