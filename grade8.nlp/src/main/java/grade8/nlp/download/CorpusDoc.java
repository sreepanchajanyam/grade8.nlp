package grade8.nlp.download;

public class CorpusDoc {

	private final String[] contentArr;
	private final float[] scoreArr;

	public CorpusDoc(String[] contentArr, float[] scoreArr) {
		this.contentArr = contentArr;
		this.scoreArr = scoreArr;
	}

	public String[] getCorpus() {
		return contentArr;
	}

	public float[] getScore() {
		return scoreArr;
	}
	
	public String getContent(){
		StringBuilder sb = new StringBuilder();
		for(String content : contentArr) {
			sb.append(content).append("\n");	
		}
		
		return sb.toString();
	}
}
